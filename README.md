# Drakgoku

Soy Drakgoku 💀

<img src="https://i.imgur.com/obzoqqd.jpeg" alt="Un gato blanco y negro" style="width: object-fit: cover; object-position: center;"/>

Soy un programador Java y Java Android.

Soy un desarrollador de videojuegos que hace poco que me adentré en este mundo y es mi pasión. Me encanta crear experiencias interactivas y visuales que sorprendan y emocionen a los jugadores. Estoy aprendiendo a usar uno de lo motores motores de juegos, lenguajes de programación y herramientas, como:


- Unreal Engine 5
- C++ y Blueprints
- Blender y Photoshop
- Git, GitHub y GitLab

Puedes ver algunos de mis videojuegos más destacados en mi perfil de GitHub. Aquí te dejo una muestra de ellos:

| Nombre | Descripción | Tecnologías | URL |
| ------ | ----------- | ----------- | ----------- | 
| Sin título | Actualmente en desarrollo | Unreal Engine 5, C++, Blueprints, MongoDB, AWS | [Game](https://www.google.es)  

Por cierto, dejo una GUÍA de buenas prácticas sobre Unreal Engine Clean Architecture [Hexagonal, DDD, Vertical Slices] CRUD – Drakgoku

<a href="https://ce3dd.wordpress.com/2024/02/01/https-i-imgur-com-pae34zb-png/" > Unreal Engine Clean Architecture [Hexagonal, DDD, Vertical Slices] - Drakgoku </a>

<br/><br/><br/>

Nota: estoy en GitLab ya que GITHUB ofrece menos espacio y servicios en la capa gratuita

Por ejemplo en GitHub:
<b> remote: error: File /../../.psd is 176.50 MB; this exceeds GitHub's file size limit of 100.00 MB <b> 



